﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media.Animation;
using System.Windows.Threading;

namespace Ziuchkovski.Controls.Toast {

    /// <summary>
    /// Interaction logic for Toast.xaml
    /// </summary>
    public partial class Toast : Window, INotifyPropertyChanged {
        private object m_ContentViewModel;
        public object ContentViewModel {
            get { return m_ContentViewModel; }
            set { m_ContentViewModel = value; NotifyPropertyChanged("ContentViewModel"); }
        }

        private int _rightMargin = 20;
        private readonly int _bottomMargin = new TaskbarHelper.Taskbar().Bounds.Height + 20;

        public Toast(int delayInSeconds, object viewModel) {
            ContentViewModel = viewModel;

            DataContext = this;
            InitializeComponent();

            var timer = new DispatcherTimer { Interval = new TimeSpan(0, 0, delayInSeconds) };
            timer.Tick += (o, e) => Close();
            timer.Start();
        }

        private void Toast_Loaded(object sender, RoutedEventArgs e) {
            Dispatcher.BeginInvoke(new Action(() => {
                Top = SystemParameters.PrimaryScreenHeight - Height - _bottomMargin;
                Left = SystemParameters.PrimaryScreenWidth - Width - _rightMargin;
            }), DispatcherPriority.ApplicationIdle, null);
        }

        private void Window_Closing(object sender, CancelEventArgs e) {
            Closing -= Window_Closing;
            e.Cancel = true;
            var anim = (DoubleAnimation)FindResource("FadeOutAnimation");
            anim.Completed += (s, _) => Close();
            BeginAnimation(OpacityProperty, anim);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}