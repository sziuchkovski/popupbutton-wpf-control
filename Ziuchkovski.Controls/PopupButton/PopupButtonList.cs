﻿using System.ComponentModel;
using System.Windows;

namespace Ziuchkovski.Controls.PopupButton {

    public class PopupButtonList : DependencyObject {
        public static readonly DependencyProperty HeaderProperty = DependencyProperty.Register("Header", typeof(string), typeof(PopupButtonList));
        [Category("Common"), Description("The header displayed above the list's items.")]
        public string Header {
            get { return (string)GetValue(HeaderProperty); }
            set { SetValue(HeaderProperty, value); }
        }

        public static readonly DependencyProperty ItemsProperty = DependencyProperty.Register("Items", typeof(PopupButtonListItemCollection), typeof(PopupButtonList));
        [Category("Common"), Description("The items in the list.")]
        public PopupButtonListItemCollection Items {
            get { return (PopupButtonListItemCollection)GetValue(ItemsProperty); }
            set { SetValue(ItemsProperty, value); }
        }
    }
}