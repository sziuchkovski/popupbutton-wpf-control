﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Ziuchkovski.Controls.PopupButton {

    /// <summary>
    /// Interaction logic for PopupButton.xaml
    /// </summary>
    public partial class PopupButton : UserControl {
        public static readonly DependencyProperty PromptProperty = DependencyProperty.Register("Prompt", typeof(string), typeof(PopupButton));
        [Category("Common"), Description("The prompt to display above the available selections.")]
        public string Prompt {
            get { return (string)GetValue(PromptProperty); }
            set { SetValue(PromptProperty, value); }
        }

        public static readonly DependencyProperty ItemListsProperty = DependencyProperty.Register("Lists", typeof(PopupButtonListCollection), typeof(PopupButton));
        [Category("Common"), Description("The list of PopupButtonLists to display to the user to choose from.")]
        public PopupButtonListCollection Lists {
            get { return (PopupButtonListCollection)GetValue(ItemListsProperty); }
            set { SetValue(ItemListsProperty, value); }
        }

        public static readonly DependencyProperty SelectedItemProperty = DependencyProperty.Register("SelectedItem", typeof(string), typeof(PopupButton), new FrameworkPropertyMetadata(null, OnSelectedItemChanged));
        [Category("Common"), Description("The currently selected item.")]
        public string SelectedItem {
            get { return (string)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        private static void OnSelectedItemChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var newValue = (string)e.NewValue;
            if (!((PopupButton)d).IsNullSelectedItemAllowed && newValue == null)
                throw new ArgumentNullException("SelectedItem cannot be null because IsNullSelectedItemAllowed is false.");
        }

        public static readonly DependencyProperty IsNullSelectedItemAllowedProperty = DependencyProperty.Register("IsNullSelectedItemAllowed", typeof(bool), typeof(PopupButton), new FrameworkPropertyMetadata(true, OnIsNullSelectedItemAllowedChanged));
        [Category("Common"), Description("Is the SelectedItem property allowed to be null?")]
        public bool IsNullSelectedItemAllowed {
            get { return (bool)GetValue(IsNullSelectedItemAllowedProperty); }
            set { SetValue(IsNullSelectedItemAllowedProperty, value); }
        }

        private static void OnIsNullSelectedItemAllowedChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var newValue = (bool)e.NewValue;
            if (newValue && ((PopupButton)d).SelectedItem == null)
                throw new ArgumentNullException("IsNullSelectedItemAllowed cannot be set to true while SelectedItem is null.");
        }

        public static readonly DependencyProperty HighlightBrushProperty = DependencyProperty.Register("HighlightBrush", typeof(Brush), typeof(PopupButton), new PropertyMetadata(Brushes.LightBlue));
        [Category("Common"), Description("The background of the item under the mouse cursor.")]
        public Brush HighlightBrush {
            get { return (Brush)GetValue(HighlightBrushProperty); }
            set { SetValue(HighlightBrushProperty, value); }
        }

        public static readonly DependencyProperty TextBrushProperty = DependencyProperty.Register("TextBrush", typeof(Brush), typeof(PopupButton), new PropertyMetadata(Brushes.Black));
        [Category("Common"), Description("The color of the text.")]
        public Brush TextBrush {
            get { return (Brush)GetValue(TextBrushProperty); }
            set { SetValue(TextBrushProperty, value); }
        }

        public static readonly DependencyProperty HeaderBackgroundBrushProperty = DependencyProperty.Register("HeaderBackgroundBrush", typeof(Brush), typeof(PopupButton), new PropertyMetadata(Brushes.LightGray));
        [Category("Common"), Description("Background of list headers.")]
        public Brush HeaderBackgroundBrush {
            get { return (Brush)GetValue(HeaderBackgroundBrushProperty); }
            set { SetValue(HeaderBackgroundBrushProperty, value); }
        }

        public static readonly DependencyProperty HeaderTextBrushProperty = DependencyProperty.Register("HeaderTextBrush", typeof(Brush), typeof(PopupButton), new PropertyMetadata(Brushes.Black));
        [Category("Common"), Description("Background of list headers.")]
        public Brush HeaderTextBrush {
            get { return (Brush)GetValue(HeaderTextBrushProperty); }
            set { SetValue(HeaderTextBrushProperty, value); }
        }

        public new static readonly DependencyProperty BorderBrushProperty = DependencyProperty.Register("BorderBrush", typeof(Brush), typeof(PopupButton), new PropertyMetadata(Brushes.Black));
        [Category("Common"), Description("Color to draw the border with.")]
        public new Brush BorderBrush {
            get { return (Brush)GetValue(BorderBrushProperty); }
            set { SetValue(BorderBrushProperty, value); }
        }

        public static readonly DependencyProperty BackgroundBrushProperty = DependencyProperty.Register("BackgroundBrush", typeof(Brush), typeof(PopupButton), new PropertyMetadata(Brushes.White));
        [Category("Common"), Description("Background color of an item that does not have the mouse cursor over it.")]
        public Brush BackgroundBrush {
            get { return (Brush)GetValue(BackgroundBrushProperty); }
            set { SetValue(BackgroundBrushProperty, value); }
        }

        public PopupButton() {
            InitializeComponent();
            Lists = new PopupButtonListCollection();
            DataContext = this;
        }

        private void TheButton_Click(object sender, RoutedEventArgs e) {
            ThePopup.IsOpen = true;
        }

        private void ThePopup_MouseLeave(object sender, MouseEventArgs e) {
            ThePopup.IsOpen = false;
        }

        private void ItemBorder_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) {
            var border = sender as Border;
            var textBlock = border?.Child as TextBlock;
            if (textBlock == null)
                return;
            ThePopup.IsOpen = false;
            SelectedItem = textBlock.Text;
        }
    }
}