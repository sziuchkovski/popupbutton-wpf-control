PopupTest
=========

This is a WPF control that will act like a ComboBox, but display a list of lists in its popup view. 

Each list can be of a different length, and displays on the screen with the items listed below the list header. 
The item under the mouse cursor becomes highlighted. Moving the mouse cursor out of the boundaries of the 
popup automatically closes it. When closed, the selected item is displayed. 

Planned
---------

* Get away from strings as the headers and items and allow any arbitrary object. Allow header and item templates
  to be set just like how a ListBox works.
* Get SelectedItem to work more like a ListBox might work. Maybe remove the IsNullSelectedItemAllowed property because it doesn't really make sense. Maybe I'll make a property that contains the "default" value if nothing else is selected.

class PopupButton : UserControl
-----------------

Properties:

* bool IsNullSelectedItemAllowed: If this is true, SelectedItem may be null;
* string SelectedItem: This is the currently selected item.
* PopupButtonListCollection Lists: This is the list of lists.

Colors can be customized:

* BackgroundBrush - The background color of an item without the mouse cursor over it.
* HighlightBrush - The background color of the item under the mouse cursor.
* TextBrush - The color of the text of an item.
* BorderBrush - The color of the control's border and gridlines.
* HeaderBackgroundBrush - The background color of the list headers.
* HeaderTextBrush - The color of the text of a header.

class PopupButtonList : object
---------------------

Properties:

* string Header: The header to display above the list.
* PopupButtonListItemCollection Items: The items available for selection.

Collection Classes
-------------

These are simply "aliases" for some ObservableCollections. This makes declaring lists and list items in XAML a
bit easier since XAML doesn't support generic types very well.

* class PopupButtonListCollection : ObservableCollection<PopupButtonList>
* class PopupButtonListItemCollection : ObservableCollection<string>
