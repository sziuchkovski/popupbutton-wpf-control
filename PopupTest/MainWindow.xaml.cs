﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using Ziuchkovski.Controls.Toast;

namespace PopupTest {

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged {
        public ICommand ShowAToastCommand { get; set; }

        private string m_ToastMessage = "Toast anything!";
        public string ToastMessage {
            get { return m_ToastMessage; }
            set { m_ToastMessage = value; NotifyPropertyChanged("ToastMessage"); }
        }

        public MainWindow() {
            InitializeComponent();
            ShowAToastCommand = new RelayCommand(OnShowAToastCommand);
            DataContext = this;
        }

        private void OnShowAToastCommand(object o) {
            new Toast(3, ToastMessage).Show();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }

    public class RelayCommand : ICommand {
        public Action<object> OnExecute;

        public RelayCommand(Action<object> callback) {
            OnExecute = callback;
        }

        public bool CanExecute(object parameter) {
            return true;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter) {
            OnExecute(parameter);
        }
    }
}